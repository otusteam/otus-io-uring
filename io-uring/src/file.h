#ifndef _FILE_H_
#define _FILE_H_

#include <sys/types.h>
#include <khash.h>


typedef struct
{
    int fd;
    off_t size;
} file_t;

KHASH_MAP_INIT_STR(file_map_t, file_t*)

extern khash_t(file_map_t)* file_map;

extern file_t* get_file(const char* path);

#endif // _FILE_H_
