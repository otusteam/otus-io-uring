#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdnoreturn.h>

#define LIKELY(x)      __builtin_expect(!!(x), 1)
#define UNLIKELY(x)    __builtin_expect(!!(x), 0)


extern noreturn void die(const char* message);

#endif  // _UTILS_H_
